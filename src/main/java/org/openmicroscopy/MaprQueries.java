package org.openmicroscopy;

import org.openmicroscopy.verticles.MaprVerticle;

/**
 * Bunch of queries used by the {@link MaprVerticle}
 */
public class MaprQueries {

    public static String mapAnnotations(String where) {
        String q = "select mv.value as value, " +
                "count(distinct i.id) as imgCount, " +
                "count(distinct sl.parent.id) as childCount1, " +
                "count(distinct pdl.parent.id) as childCount2 from ImageAnnotationLink ial " +
                "join ial.child a " +
                "join a.mapValue mv " +
                "join ial.parent i " +
                "left outer join i.wellSamples ws " +
                "left outer join ws.well w " +
                "left outer join w.plate pl " +
                "left outer join pl.screenLinks sl " +
                "left outer join i.datasetLinks dil " +
                "left outer join dil.parent ds " +
                "left outer join ds.projectLinks pdl " +
                "where %s " +
                "AND ( " +
                "(dil is null and ds is null and pdl is null and ws is not null and w is not null and pl is not null and sl is not null) " +
                "OR (ws is null and w is null and pl is null and sl is null and dil is not null and ds is not null and pdl is not null) " +
                ") group by mv.value order by count(distinct i.id) DESC";
        return String.format(q, where);
    }

    public static String screens(String where) {
        String query = "select new map(mv.value as value, " +
                "screen.id as id, " +
                "screen.name as name, " +
                "screen.details.owner.id as ownerId, " +
                "screen as screen_details_permissions, " +
                "count(distinct pl.id) as childCount, " +
                "count(distinct ws.id) as imgCount) " +
                "from WellAnnotationLink wal join wal.child a join a.mapValue mv " +
                "join wal.parent w join w.wellSamples ws " +
                "join w.plate pl join pl.screenLinks sl " +
                "join sl.parent screen " +
                "where %s " +
                "group by screen.id, screen.name, mv.value " +
                "order by lower(screen.name), screen.id";
        return String.format(query, where);
    }

    public static String projects(String where) {
        String query = "select new map(mv.value as value, " +
                " project.id as id, " +
                " project.name as name, " +
                " project.details.owner.id as ownerId, " +
                " project as project_details_permissions, " +
                " count(distinct dataset.id) as childCount, " +
                " count(distinct i.id) as imgCount) " +
                " from ImageAnnotationLink ial join ial.child a join a.mapValue mv " +
                " join ial.parent i join i.datasetLinks dil " +
                " join dil.parent dataset join dataset.projectLinks pl " +
                " join pl.parent project " +
                " where %s " +
                " group by project.id, project.name, mv.value " +
                " order by lower(project.name), project.id";
        return String.format(query, where);
    }

    public static String plates(String where) {
        String query = "select mv.value as value, " +
                "plate.id as id, " +
                "plate.name as name, " +
                "plate.details.owner.id as ownerId, " +
                "plate as plate_details_permissions, " +
                "count(distinct ws.id) as childCount " +
                "from WellAnnotationLink wal join wal.child a join a.mapValue mv " +
                "join wal.parent w join w.wellSamples ws " +
                "join w.plate plate join plate.screenLinks sl " +
                "join sl.parent screen " +
                "where %s " +
                "group by plate.id, plate.name, mv.value " +
                "order by lower(plate.name), plate.id, mv.value";
        return String.format(query, where);
    }

    public static String search(String where) {
        final String query = "select mv.value as value " +
                "from ImageAnnotationLink ial join ial.child a join a.mapValue mv " +
                "where %s and mv.value like :query " +
                "group by mv.value " +
                "order by length(mv.value) ASC, " +
                "mv.value ASC";
        return String.format(query, where);
    }

}
