package org.openmicroscopy.controllers;

import org.openmicroscopy.Utils;
import org.openmicroscopy.verticles.MaprVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Handles all HTTP requests for mapr API endpoints
 */
public class MaprController {

    public static final String API_MAPR = "/mapr";

    public static final String API_AUTOCOMPLETE = "/autocomplete";

    public static final String API_ANNOTATIONS = "/annotations";

    public static final String API_SCREENS = "/screens";

    public static final String API_PROJECTS = "/projects";

    public static final String API_PLATES = "/plates";

    private static final Logger log =
            LoggerFactory.getLogger(MaprController.class);

    private Vertx vertx;
    private Router router;

    public MaprController(Vertx vertx) {
        this.vertx = vertx;
    }

    public Router getRouter() {
        if (router == null) {
            router = Router.router(vertx);
            router.get(API_AUTOCOMPLETE).handler(this::getSearch);
            router.get(API_ANNOTATIONS).handler(this::getAnnotations);
            router.get(API_SCREENS).handler(this::getScreens);
            router.get(API_PROJECTS).handler(this::getProjects);
            router.get(API_PLATES).handler(this::getPlates);
        }

        return router;
    }

    private void getSearch(RoutingContext rc) {
        getRequest(MaprVerticle.GET_SEARCH_EVENT, rc);
    }

    private void getAnnotations(RoutingContext rc) {
        getRequest(MaprVerticle.GET_MAP_ANNOTATIONS_EVENT, rc);
    }

    private void getScreens(RoutingContext rc) {
        getRequest(MaprVerticle.GET_SCREENS_EVENT, rc);
    }

    private void getProjects(RoutingContext rc) {
        getRequest(MaprVerticle.GET_PROJECTS_EVENT, rc);
    }

    private void getPlates(RoutingContext rc) {
        getRequest(MaprVerticle.GET_PLATES_EVENT, rc);
    }

    private void getRequest(String event, RoutingContext rc) {
        JsonObject json = Utils.handleSession(rc)
                .mergeIn(maprRequestToJson(rc.request()));

        vertx.eventBus().send(event, json.encode(),
                new StandardResponse(rc.response()));
    }

    private JsonObject maprRequestToJson(HttpServerRequest req) {
        JsonObject json = new JsonObject();
        json.put("value", req.getParam("value"));

        String mapNamespace = req.getParam("namespaces");
        if (mapNamespace != null && !mapNamespace.isEmpty()) {
            List<String> names = Arrays.stream(mapNamespace.split(","))
                    .map(String::trim)
                    .collect(Collectors.toList());
            json.put("namespaces", new JsonArray(names));
        }

        String mapAnonNameParam = req.getParam("annotations");
        if (mapAnonNameParam != null && !mapAnonNameParam.isEmpty()) {
            List<String> names = Arrays.stream(mapAnonNameParam.split(","))
                    .map(String::trim)
                    .collect(Collectors.toList());
            json.put("annotations", new JsonArray(names));
        }

        return json;
    }

    private class StandardResponse implements Handler<AsyncResult<Message<String>>> {

        private HttpServerResponse response;

        public StandardResponse(HttpServerResponse response) {
            this.response = response;
        }

        @Override
        public void handle(AsyncResult<Message<String>> result) {
            try {
                if (result.failed()) {
                    // setError(response, result.cause());
                    return;
                }
                Message<String> res = result.result();
                JsonObject data = new JsonObject(res.body());
                String reply = data.encode();

                response.headers()
                        .set("Content-Type", "application/json")
                        .set("Content-Length", String.valueOf(reply.length()));
                response.write(reply);
            } finally {
                response.end();
                log.debug("Response ended");
            }
        }
    }

}
