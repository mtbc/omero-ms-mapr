/*
 * Copyright (C) 2017 Glencoe Software, Inc. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package org.openmicroscopy.verticles;

import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.eventbus.ReplyException;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CookieHandler;
import io.vertx.ext.web.handler.SessionHandler;
import io.vertx.ext.web.sstore.LocalSessionStore;
import ome.util.messages.ShutdownMessage;
import org.openmicroscopy.controllers.LoginController;
import org.openmicroscopy.controllers.MaprController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;


/**
 * Main entry point for the OMERO thumbnail Vert.x microservice server.
 *
 * @author Chris Allan <callan@glencoesoftware.com>
 * <p>
 * com.gozisoft.HttpServerVerticle
 */
public class HttpServerVerticle extends AbstractVerticle {

    private static final Logger log =
            LoggerFactory.getLogger(HttpServerVerticle.class);

    private ApplicationContext appContext;

    public HttpServerVerticle(ApplicationContext context) {
        this.appContext = context;
    }


    /**
     * Entry point method which starts the server event loop and initializes
     * our current OMERO.web session store.
     */
    @Override
    public void start(Future<Void> future) {
        log.info("Starting verticle");

        ConfigStoreOptions store = new ConfigStoreOptions()
                .setType("file")
                .setFormat("json")
                .setConfig(new JsonObject()
                        .put("path", "src/main/conf/vertx.json")
                )
                .setOptional(true);

        ConfigRetrieverOptions options = new ConfigRetrieverOptions()
                .setIncludeDefaultStores(false)
                .addStore(store);

        ConfigRetriever retriever = ConfigRetriever.create(vertx, options);

        retriever.getConfig(ar -> {
            try {
                deploy(ar.result(), future);
            } catch (Exception e) {
                future.fail(e);
            }
        });
    }

    /**
     * Exit point method which when the verticle stops, cleans up our current
     * OMERO.web session store.
     */
    @Override
    public void stop() {
        // TODO: This is copied from Entry.shutdown which should be refactored
        // to OmeroContext
        int rc = 0;
        try {
            appContext.publishEvent(new ShutdownMessage(this));
            // appContext.closeAll();
        } catch (Throwable t) {
            log.error("Error shutting down", t);
            rc = 3;
        }
        System.exit(rc);
    }

    /**
     * Deploys our verticles and performs general setup that depends on
     * configuration.
     *
     * @param config Current configuration
     */
    public void deploy(JsonObject config, Future<Void> future) {
        log.info("Deploying verticle");

        // Deployment options
        DeploymentOptions deployOpts = new DeploymentOptions()
                .setWorker(true)
                .setMultiThreaded(true)
                .setConfig(config);

        // Deploy Verticles
        vertx.deployVerticle(new LoginVerticle(appContext), deployOpts);
        vertx.deployVerticle(new MaprVerticle(appContext), deployOpts);

        // Setup HTTP
        HttpServer server = vertx.createHttpServer();
        Router router = Router.router(vertx);

        // Cookie handler so we can pick up the OMERO.web session
        router.route().handler(BodyHandler.create());
        router.route().handler(CookieHandler.create());
        router.route().handler(SessionHandler.create(LocalSessionStore.create(vertx)));

        // Login routing
        LoginController loginController = new LoginController(vertx);
        Router loginRouter = loginController.getRouter();
        router.mountSubRouter(LoginController.API_LOGIN, loginRouter);

        // Mapr routing
        MaprController maprController = new MaprController(vertx);
        Router maprRouter = maprController.getRouter();
        router.mountSubRouter(MaprController.API_MAPR, maprRouter);

        int port = config.getInteger("http.port");
        server.requestHandler(router::accept).listen(port, res -> {
            if (res.succeeded()) {
                log.debug("Server listening on port " + port);
                future.complete();
            } else {
                log.debug("Failed to launch server");
                future.fail(res.cause());
            }
        });
    }

    private void setError(HttpServerResponse response, Throwable cause) {
        int statusCode = 404;
        if (cause instanceof ReplyException) {
            statusCode = ((ReplyException) cause).failureCode();
        }
        response.setStatusCode(statusCode);
    }
}
