package org.openmicroscopy;

import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import ome.system.OmeroContext;
import org.openmicroscopy.verticles.HttpServerVerticle;
import org.springframework.context.ApplicationContext;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

public class Application {

    private static void setSystemProperties(String[] filenames) throws IOException {
        final Properties propertiesSystem = System.getProperties();
        for (final String filename : filenames) {
            final Properties propertiesNew = new Properties();
            try (final InputStream filestream = new FileInputStream(filename)) {
                propertiesNew.load(filestream);
            }
            propertiesSystem.putAll(propertiesNew);
        }
    }

    public static void main(String[] args) {
        Vertx vertx = Vertx.vertx();

        ConfigStoreOptions file = new ConfigStoreOptions()
                .setType("file")
                .setFormat("json")
                .setConfig(new JsonObject().put("path", "src/main/conf/omero.json"));

        ConfigRetrieverOptions options = new ConfigRetrieverOptions()
                .setIncludeDefaultStores(false)
                .addStore(file);

        ConfigRetriever retriever = ConfigRetriever.create(vertx, options);

        retriever.getConfig(ar -> {
            applyConfig(ar.result());
            ApplicationContext context = OmeroContext.getManagedServerContext();
            vertx.deployVerticle(new HttpServerVerticle(context));
        });
    }

    private static void applyConfig(JsonObject config) {
        for (Map.Entry<String, Object> it : config) {
            System.setProperty(it.getKey(), String.valueOf(it.getValue()));
        }
    }
}