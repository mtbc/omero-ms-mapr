package org.openmicroscopy.verticles;

import org.openmicroscopy.MaprQueries;
import org.openmicroscopy.ViewModels.MapAnnotation;
import org.openmicroscopy.ViewModels.Plate;
import org.openmicroscopy.Work;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import ome.api.IQuery;
import ome.parameters.Parameters;
import ome.services.util.Executor;
import ome.system.Principal;
import ome.system.ServiceFactory;
import org.springframework.context.ApplicationContext;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MaprVerticle extends AbstractVerticle {

    public static String GET_SEARCH_EVENT = "omero.get_simple";

    public static String GET_MAP_ANNOTATIONS_EVENT = "omero.get_map_annotations";

    public static String GET_SCREENS_EVENT = "omero.get_screens";

    public static String GET_PROJECTS_EVENT = "omero.get_projects";

    public static String GET_PLATES_EVENT = "omero.get_plates";

    private static final Map<String, String> ALL_GROUPS = new HashMap<>();

    static {
        ALL_GROUPS.put("omero.group", "-1");
    }

    private ApplicationContext omeroContext;

    public MaprVerticle(ApplicationContext ctx) {
        this.omeroContext = ctx;
    }

    @Override
    public void start() throws Exception {
        super.start();

        vertx.eventBus().consumer(GET_SEARCH_EVENT, this::getSimple);
        vertx.eventBus().consumer(GET_MAP_ANNOTATIONS_EVENT, this::getMapAnnotationNames);
        vertx.eventBus().consumer(GET_SCREENS_EVENT, this::getScreens);
        vertx.eventBus().consumer(GET_PROJECTS_EVENT, this::getProjects);
        vertx.eventBus().consumer(GET_PLATES_EVENT, this::getPlates);
    }

    @Override
    public void stop() throws Exception {
        super.stop();
    }

    public void getSimple(Message<String> msg) {
        JsonObject data = new JsonObject(msg.body());
        String session = data.getString("sessionId");
        String value = data.getString("value");
        List<String> namespaces = jsonArrayToStringList(
                data.getJsonArray("namespaces"));
        List<String> mapNames = jsonArrayToStringList(
                data.getJsonArray("annotations"));

        Parameters params = new Parameters()
                .addString("query", String.format("%s%%", value))
                .addList("ns", namespaces)
                .addList("filter", mapNames);

        List<String> whereParts = whereBits(params);
        whereParts.add("mv.value != ''");
        whereParts.add("mv.value like :query");

        String query = MaprQueries.search(
                String.join(" and ", whereParts)
        );

        List<String> values = execute(session, new Work<List<String>>() {
            @Override
            public List<String> doWork(ServiceFactory sf) {
                return sf.getQueryService().projection(query, params)
                        .stream()
                        .map(item -> (String) item[0])
                        .collect(Collectors.toList());
            }
        });

        JsonObject results = new JsonObject()
                .put("values", new JsonArray(values));

        msg.reply(results.encode());
    }

    public void getMapAnnotationNames(Message<String> msg) {
        JsonObject data = new JsonObject(msg.body());
        String session = data.getString("sessionId");
        Parameters params = buildCommonParams(data);

        List<MapAnnotation> values = execute(session, new Work<List<MapAnnotation>>() {
            @Override
            public List<MapAnnotation> doWork(ServiceFactory sf) {
                return _getMapAnnotationNames(sf.getQueryService(), params);
            }
        });

        // Convert MapAnnotation to List of JsonObjects
        List<JsonObject> mapAnnotations = values.stream()
                .map(JsonObject::mapFrom)
                .collect(Collectors.toList());

        // Convert List to JsonArray
        JsonObject results = new JsonObject()
                .put("values", new JsonArray(mapAnnotations));

        // Encode as a string and reply to sender
        msg.reply(results.encode());
    }

    public void getScreens(Message<String> msg) {
        JsonObject data = new JsonObject(msg.body());
        String session = data.getString("sessionId");
        Parameters params = buildCommonParams(data);

        executor().execute(ALL_GROUPS, new Principal(session), new Work<Object>() {
            @Override
            public Object doWork(ServiceFactory sf) {
                Object res = _getScreens(sf.getQueryService(), params);
                return res;
            }
        });
    }

    public void getProjects(Message<String> msg) {
        JsonObject data = new JsonObject(msg.body());
        String session = data.getString("sessionId");
        Parameters params = buildCommonParams(data);

        execute(session, new Work<Object>() {
            @Override
            public Object doWork(ServiceFactory sf) {
                return _getProjects(sf.getQueryService(), params);
            }
        });
    }

    public void getPlates(Message<String> msg) {
        JsonObject data = new JsonObject(msg.body());
        String session = data.getString("sessionId");
        long screenId = data.getLong("screenId");

        Parameters params = buildCommonParams(data)
                .addLong("sid", screenId);

        List<Plate> plates = execute(session, new Work<List<Plate>>() {
            @Override
            public List<Plate> doWork(ServiceFactory sf) {
                return _getPlates(sf.getQueryService(), params);
            }
        });

        // Convert Plates to List of JsonObjects
        List<JsonObject> mapAnnotations = plates.stream()
                .map(JsonObject::mapFrom)
                .collect(Collectors.toList());

        // Convert List to JsonArray
        JsonObject results = new JsonObject()
                .put("values", new JsonArray(mapAnnotations));

        // Encode as a string and reply to sender
        msg.reply(results.encode());
    }

    private Parameters buildCommonParams(JsonObject data) {
        String mapValue = data.getString("value");
        List<String> namespaces = jsonArrayToStringList(
                data.getJsonArray("namespaces"));
        List<String> mapNames = jsonArrayToStringList(
                data.getJsonArray("annotations"));

        return new Parameters()
                .addString("value", mapValue)
                .addList("ns", namespaces)
                .addList("filter", mapNames);
    }

    private List<MapAnnotation> _getMapAnnotationNames(IQuery iQuery, Parameters params) {
        List<String> where = whereBits(params);
        String query = MaprQueries.mapAnnotations(String.join(" and ", where));
        List<Object[]> res = iQuery.projection(query, params);
        return res.stream().map(objects -> {
            final String id = String.valueOf(objects[0]);
            final long imageCount = Long.valueOf(
                    String.valueOf(objects[1])
            );
            final long childCount1 = Long.valueOf(
                    String.valueOf(objects[2])
            );
            final long childCount2 = Long.valueOf(
                    String.valueOf(objects[3])
            );

            MapAnnotation ma = new MapAnnotation();
            ma.name = String.format("%s (%d)", id, imageCount);
            ma.id = id;
            ma.ownerId = 0;
            ma.childCount = childCount1 + childCount2;
            ma.extra = Collections.singletonMap("counter", imageCount);  //new Tuple<>("counter", imageCount);
            return ma;
        }).collect(Collectors.toList());
    }

    private List<Object[]> _getScreens(IQuery iQuery, Parameters params) {
        List<String> where = whereBits(params);
        String query = MaprQueries.projects(String.join(" and ", where));
        return iQuery.projection(query, params);
    }

    private Object _getProjects(IQuery iQuery, Parameters params) {
        List<String> where = whereBits(params);
        String query = MaprQueries.projects(String.join(" and ", where));
        return iQuery.projection(query, params);
    }

    private List<Plate> _getPlates(IQuery iQuery, Parameters params) {
        List<String> where = whereBits(params);
        where.add("screen.id = :sid");

        String query = MaprQueries.plates(String.join(" and ", where));
        return iQuery.projection(query, params).stream()
                .map(objects -> {
                    String value = String.valueOf(objects[0]);
                    long id = Value.ToLong(objects[1]);
                    String name = String.valueOf(objects[2]);
                    long ownerId = Value.ToLong(objects[3]);
                    long childCount = Value.ToLong(objects[5]);

                    Plate plate = new Plate();
                    plate.name = name;
                    plate.id = id;
                    plate.ownerId = ownerId;
                    plate.childCount = childCount;
                    plate.extra = new HashMap<>();
                    plate.extra.put("node", "plate");
                    plate.extra.put("value", value);
                    return plate;
                })
                .collect(Collectors.toList());
    }

    private List<String> whereBits(Parameters params) {
        // Build where clause
        List<String> where = new ArrayList<>();

        if (params.get("filter") != null) {
            where.add("mv.name in (:filter)");
        }

        if (params.get("ns") != null) {
            where.add("a.ns in (:ns)");
        }

        if (params.get("value") != null) {
            where.add("mv.value = :value");
        }

        return where;
    }

    private List<String> jsonArrayToStringList(JsonArray jsonArray) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < jsonArray.size(); i++) {
            list.add(jsonArray.getString(i));
        }
        return list;
    }

    private <T> T execute(String sessionId, Work<T> work) {
        return executor().execute(ALL_GROUPS, new Principal(sessionId), work);
    }

    private Executor executor() {
        return (Executor) omeroContext.getBean("executor");
    }

}

/**
 * Little helper class for converting object types
 */
class Value {
    public static long ToLong(Object object) {
        return Long.parseLong(String.valueOf(object));
    }
}
