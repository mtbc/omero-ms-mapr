package org.openmicroscopy;

import io.vertx.core.json.DecodeException;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

public class Utils {

    /**
     * Adds session id to message body
     *
     * @param rc current routing context
     * @return
     * @throws SecurityException thrown if no session id is found
     */
    public static JsonObject handleSession(RoutingContext rc) throws SecurityException {
        String sessionId = rc.session().get("sessionId");
        if (sessionId == null || sessionId.isEmpty()) {
            throw new SecurityException("No session active session found");
        }

        JsonObject body;
        try {
            body = rc.getBodyAsJson();
        } catch (DecodeException e) {
            body = new JsonObject();
        }
        return body.put("sessionId", sessionId);
    }
}
