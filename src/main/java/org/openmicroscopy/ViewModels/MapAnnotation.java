package org.openmicroscopy.ViewModels;


import java.util.Map;

public class MapAnnotation {
    public String id;
    public String name;
    public String permsCss;
    public long ownerId;
    public long childCount;
    public Map extra;
}
