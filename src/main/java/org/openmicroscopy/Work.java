package org.openmicroscopy;

import ome.services.util.Executor;
import ome.system.ServiceFactory;
import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;

public abstract class Work<T> implements Executor.Work<T> {
    @Override
    public String description() {
        return "";
    }

    @Transactional(readOnly = false)
    @Override
    public T doWork(Session session, ServiceFactory sf) {
        return doWork(sf);
    }

    public abstract T doWork(ServiceFactory sf);
}
